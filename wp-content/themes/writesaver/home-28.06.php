<?php
/*
 * Template Name: home
 */

$current_user = wp_get_current_user();
$user_roles_array = $current_user->roles;
$user_role = array_shift($user_roles_array);
if (is_user_logged_in()):
    if ($user_role == "customer") {
        echo '<script>window.location.href="' . get_the_permalink(762) . '"</script>';
        exit;
    } elseif ($user_role == "proofreader") {
        echo '<script>window.location.href="' . get_the_permalink(810) . '"</script>';
        exit;
    } elseif ($user_role == "administrator") {
       // echo '<script>window.location.href="' . site_url() . '/wp-admin"</script>';
       // exit;
    } else {
        
    }

endif;

get_header();
?>
<section class="top_section section" id="section0">            
    <div class="top_section_main">
        <div class="top_contant">
            <div class="container">
                <div class="wrap">
                    <div class="type-wrap">
                        <div id="typed-strings">                                
                            <p>Welcome to Writesaver</p>
                            <p>The fastest way to write with perfect English</p>
                            <p>Input your text below to get started</p>
                        </div>
                        <span id="typed"></span>
                    </div>                        
                </div>

                <div class="main_editor home">
                    <div class="editor_top">
                        <div class="editor_inner_top">                            
                            <div class="used_word">
                                <span>Word Count:</span><span class="count">0</span>
                            </div>
                        </div>
                        <div class="hidden_scroll">
                            <div class="parentscrollcontents" style="width: 100%; height:300px; display: inline-block;">
                                <div class="changeable" placeholder="Type or paste (Ctrl+V) your text here to get it corrected by our team of expert proofreaders." contenteditable="true" id="txt_area_upload_doc" style="min-height: 300px; display: inline-block; "></div>
                            </div>
                        </div>
                    </div>
                    <div class="submit_area">
                        <p>Click below to have your writing edited by our proofreading team of native English speakers. Your first <?php echo of_get_option('free_words_for_customer') ?> words is free!</p>
                        <div class="btn_blue">
                            <?php
                            $login_url = "";
                            if (!is_user_logged_in()) {
                                $login_url = get_page_link(545);
                            } else {
                                $current_user = wp_get_current_user();
                                $user_roles_array = $current_user->roles;
                                $user_role = array_shift($user_roles_array);
                                if ($user_role == 'customer') {
                                    $login_url = get_page_link(762);
                                } else if ($user_role == 'proofreader') {
                                    $login_url = get_page_link(810);
                                } else {
                                    $login_url = get_page_link(6);
                                }
                            }
                            ?>
                            <a href="javascript:void(0);" class="btn_sky proof_doc" >Have this paper proofread!</a>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section> 
<?php
if (have_posts()) :
    while (have_posts()) : the_post();
        the_content();
    endwhile;
endif;
?>

<?php get_footer(); ?>  

<script>
    $(document).ready(function () {
        $("body").addClass("home");
    });
// Get Word count
    function get_doc_word_count(val) {
        debugger;
        var wom = val.match(/\S+/g);
        return wom ? wom.length : 0;
    }

    $('#txt_area_upload_doc').bind("DOMNodeInserted", function () {
        if ($(this).text().trim() == "")
        {
            $(".used_word .count").text("0");
        } else
        {
            var doc_detail = $('#txt_area_upload_doc').text().trim();
            doc_detail = doc_detail.replace(/\s+/g, ' ');
            var words = get_doc_word_count(doc_detail);
            $(".used_word .count").text(words);
            $("#txt_area_upload_doc span").removeAttr("style");
        }
    });

    $('#txt_area_upload_doc').keydown(function (e) {
        if (e.keyCode == 13) {
            document.execCommand('insertHTML', false, ' ');
        }
    });

    $('#txt_area_upload_doc').keyup(function () {
        if ($(this).text().trim() == "")
        {
            $(".used_word .count").text("0");
        } else
        {
            var doc_detail = $('#txt_area_upload_doc').text().trim();
            doc_detail = doc_detail.replace(/\s+/g, ' ');
            var words = get_doc_word_count(doc_detail);
            $(".used_word .count").text(words);
            $("#txt_area_upload_doc span").removeAttr("style");
        }
    });

    $('.proof_doc').click(function () {
        $('.spn_submited_doc_error').remove();
        var desc = $.trim($("#txt_area_upload_doc").html());
        if (desc == '') {
            $('div.submit_area').append('<p class="spn_submited_doc_error" style="color:red;"> Document should not be blank </p>');
            setTimeout(function () {
                $('.spn_submited_doc_error').fadeOut('slow');
            }, 2000);
        } else {
            localStorage.setItem('proof_doc', desc);
            window.location.href = "<?php echo $login_url; ?>";
        }
    });

    function checkTestWordCount()
    {
        var words = $('#txt_area_upload_doc').text().replace(/^[\s,.;]+/, "").replace(/[\s,.;]+$/, "").split(/[\s,.;]+/).length;

        var desc = $.trim($("#txt_area_upload_doc").text());

        var data = new FormData();
        data.append('action', 'save_user_upload_doc123');
        data.append('word_desc', desc);

        if (desc != "")
        {
            $.ajax({
                url: '<?php echo admin_url('admin-ajax.php'); ?>',
                dataType: 'text', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data,
                type: 'post',
                success: function (data) {
                    // $("#txt_area_upload_doc").text(data);
                    if (data == 'error')
                    {
                        // alert("Please enter data");
                    } else
                    {
                        // alert(data);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert("Error");
                }
            });
        } else
        {
            alert('no data found');
        }
    }


</script>