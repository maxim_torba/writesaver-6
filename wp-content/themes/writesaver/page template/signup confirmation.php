<?php
/* Template Name: Signup Confirmation */

get_header();
if (is_user_logged_in() ) {
    echo '<script>window.location.href="' . get_site_url() . '"</script>';
    exit;
}
?>
<section class="login">

    <div class="breadcum">

        <div class="container">

            <div class="page_title">

                <?php the_title('<h1>', '</h1>'); ?>

            </div>
            <div class="confirmation_thank_you registration_thank_you">
                <div class="row">
                    <div class="col-md-offset-3 col-sm-6">
                        <div class="thank-you">
                            <div class="thank_msg">
                                <div class="thank_img">
                                    <img src="<?php echo get_template_directory_uri() ?>/images/check_thanku.png" alt="images">
                                </div>
                                <?php
                                $string = $_REQUEST['string'];
                                $encrypt_method = "AES-256-CBC";
                                $secret_key = 'This is my secret key';
                                $secret_iv = 'This is my secret iv';
                                if ($string) {
// hash
                                    $key = hash('sha256', $secret_key);

// iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
                                    $iv = substr(hash('sha256', $secret_iv), 0, 16);

                                    $decrypted_string = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);

                                    if (email_exists($decrypted_string)) {
                                        ?>
                                        <h2>Your Email address has already been confirmed </h2>
                                        <p>You have already confirmed your email address. Please click the link below to login.</p> 
                                        <p><a href="javascript:void(0);" class="open_login" onclick="open_loginpop('login');">Click here to Login</a></p>
                                        <?php
                                    } else {
                                        global $wpdb;

                                        $table_name = 'wp_pending_users';

                                        $results = $wpdb->get_results("SELECT * FROM wp_pending_users WHERE email = '" . $decrypted_string . "'");
                                        if (count($results) > 0) {
                                            foreach ($results as $result):
                                                $id = $result->id;
                                                $fname = $result->fname;
                                                $lname = $result->lname;
                                                $phone = $result->phone;
                                                $pw = decrypt_string($result->password);
                                                $role = $result->role;
                                            endforeach;

                                            /* Add user */
                                            $userdata = array(
                                                'user_login' => $decrypted_string,
                                                'user_pass' => $pw,
                                                'user_email' => $decrypted_string,
                                                'first_name' => $fname,
                                                'last_name' => $lname,
                                                'role' => $role
                                            );

                                            $user_id = wp_insert_user($userdata);

                                            if ($user_id) {
                                                update_user_meta($user_id, 'role', $role);
                                                add_user_meta($user_id, '_phone_number', $phone);

                                                $wpdb->delete($table_name, array('ID' => $id));

                                                $user = get_user_by('email', $decrypted_string);

                                                $free_words = of_get_option('free_words_for_customer');
                                                if ($role == 'customer') {
                                                    $wpdb->insert(
                                                            'tbl_customer_general_info', array(
                                                        'fk_customer_id' => $user->ID,
                                                        'free_words' => $free_words,
                                                        'total_submited_docs' => 0,
                                                        'remaining_credit_words' => $free_words,
                                                        'created_date' => date('Y-m-d H:i:s')
                                                            )
                                                    );

                                                    $wpdb->insert(
                                                            'wp_notification_settings', array(
                                                        'user_id' => $user->ID,
                                                        'dash_doc_started' => 1,
                                                        'dash_doc_completed' => 1,
                                                        'receive_doc_started' => 1,
                                                        'receive_doc_completed' => 1,
                                                        'receive_stories' => 1,
                                                        'createddate' => date('Y-m-d H:i:s')
                                                            )
                                                    );
                                                } elseif ($role == 'proofreader') {
                                                    $wpdb->insert(
                                                            'wp_proofreader_notification_setting', array(
                                                        'user_id' => $user->ID,
                                                        'desktop_new_doc' => 1,
                                                        'desktop_other_notification' => 1,
                                                        'email_info_about_availability' => 1,
                                                        'email_relevant_stories' => 1,
                                                        'created_date' => date('Y-m-d H:i:s')
                                                            )
                                                    );
                                                }

                                                /* Email to admin */
                                                $site_admin_email = get_bloginfo('admin_email');
                                                $to = $site_admin_email;
                                                $subject = '[' . get_bloginfo('name') . '] New User Registration';
                                                $msg = '';
                                                $msg .= 'New user registration on your site Writesaver: <br/><br/>';
                                                $msg .= 'Email: ' . $decrypted_string . '<br/><br/>';
                                                $msg .= 'Role: ' . $role . '<br/><br/>';


                                                $html = '<html>
    <head><style> @media screen and (max-width: 601px) {
	.gmail_wrap {
		width: 600px!important;
	}
        
}
 @media screen and (min-width: 1499px) {
	.mail_wrap {
		width: 100%!important;
	}
        
}</style> </head>
    <body><div style="width: 850px; margin: 0 auto" class="gmail_wrap">
        <div style="width: 250px; margin: 0 auto">
        <a style="margin: 0 15px 0px 0; width: 250px" href="' . get_site_url() . '" onclick="return false" rel="noreferrer">
            <img src="' . of_get_option('header_logo') . '" alt="logo" style="width: 100%">                
        </a>    
        </div>
    <div class="mail_wrap" style="background: #f2f0f1; padding: 20px; border-radius: 15px; margin: 20px 0; diplay: inline-block; width: auto">           
        <div style="width: 100%; display: inline-block; margin-bottom: 30px">
            <h2 style="color: #0071bd; font-size: 13px; text-transform: capitalize;">Hi admin,</h2>
            <div >
                <span style=" color: #333; margin: 0; width: 100%; display: inline-block; padding: 8px 5px">
                    ' . $msg . '
                </span>
            </div>            
        </div>
        <div style="width: 100%; display: inline-block; margin-bottom: 30px">
            <h2 style="color: #0071bd; font-size: 13px;">Thank you!</h2>
            <div style="width: 100%; ">
                <span style=" color: #333; margin: 0; width: 100%; display: inline-block; padding: 0">
                    ' . get_bloginfo() . '
                </span>
            </div>
          </div>  
          <p style="font-size: 14px; color: #7c7c7c; line-height: 22px; text-align: center; ">' . of_get_option('copyright_text') . '</p>
    </div>
</div></body>
</html>';
                                                $headers = "MIME-Version: 1.0" . "\r\n";
                                                $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
                                                $headers .= 'From: ' . get_bloginfo() . ' <contact@writesaver.co>' . "\r\n";

                                                wp_mail($to, $subject, $html, $headers);


                                                /* End email to admin */

                                                /* Email to user */
                                                $to = $decrypted_string;
                                                $subject = '[' . get_bloginfo('name') . '] Your username and password info';
                                                $msg = '';
                                                $msg .= 'Username: ' . $decrypted_string . '<br/><br/>';
                                                $msg .= 'Password: ' . $pw . '<br/><br/>';
                                                $msg .= 'Role: ' . $role . '<br/><br/>';
                                                $msg .= 'To login to your account, follow the following link' . '<br/><br/>';
                                                $msg .= '<a href="' . get_permalink(545) . '">Click here to login</a>';



                                                $html = '<html>
    <head><style> @media screen and (max-width: 601px) {
	.gmail_wrap {
		width: 600px!important;
	}
        
}
 @media screen and (min-width: 1499px) {
	.mail_wrap {
		width: 100%!important;
	}
        
}</style> </head>
    <body><div style="width: 850px; margin: 0 auto" class="gmail_wrap">
        <div style="width: 250px; margin: 0 auto">
        <a style="margin: 0 15px 0px 0; width: 250px" href="' . get_site_url() . '" onclick="return false" rel="noreferrer">
            <img src="' . of_get_option('header_logo') . '" alt="logo" style="width: 100%">                
        </a>    
        </div>
  <div class="mail_wrap" style="background: #f2f0f1; padding: 20px; border-radius: 15px; margin: 20px 0; diplay: inline-block; width: auto">           
        <div style="width: 100%; display: inline-block; margin-bottom: 30px">
            <h2 style="color: #0071bd; font-size: 13px; text-transform: capitalize;">Hi ' . $fname . ' ' . $lname . ',</h2>
            <div >
                <span style=" color: #333; margin: 0; width: 100%; display: inline-block; padding: 8px 5px">
                    ' . $msg . '
                </span>
            </div>            
        </div>
        <div style="width: 100%; display: inline-block; margin-bottom: 30px">
            <h2 style="color: #0071bd; font-size: 13px;">Thank you!</h2>
            <div style="width: 100%; ">
                <span style=" color: #333; margin: 0; width: 100%; display: inline-block; padding: 0">
                    ' . get_bloginfo() . '
                </span>
            </div>
          </div>  
          <p style="font-size: 14px; color: #7c7c7c; line-height: 22px; text-align: center; ">' . of_get_option('copyright_text') . '</p>
    </div>
</div></body>
</html>';
                                                $headers = "MIME-Version: 1.0" . "\r\n";
                                                $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
                                                $headers .= 'From: ' . get_bloginfo() . ' <contact@writesaver.co>' . "\r\n";

                                               // wp_mail($to, $subject, $html, $headers);
                                                ?>
                                                <h2>Thank You For Your Confirmation</h2>
                                                <p>You have successfully confirmed your email address. Please click the link below to login.</p> 
                                                <p><a href="javascript:void(0);" class="open_login" onclick="open_loginpop('login');">Click here to Login</a></p>
                                                <?php
                                            } else {
                                                ?>
                                                <h2 >User not allowed. Please try again.</h2>                   
                                                <?php
                                            }
                                        } else {
                                            ?>
                                            <h2>Please register your email address</h2>
                                            <p>Please register your email address. Please click the link below to register.</p> 
                                             <p><a href="javascript:void(0);" class="open_login" onclick="open_loginpop('register');">Click here to register</a></p>
                                            <?php
                                        }
                                    }
                                } else {
                                    echo '<script>window.location.href="' . get_site_url() . '"</script>';
                                    exit;
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>

</section>

<?php get_footer(); ?>