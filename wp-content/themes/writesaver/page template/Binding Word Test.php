<?php

/*
 * Template Name: Binding Word Test
 */

get_header();
?>
<style>.highlight {
        background-color: yellow;
        color: green;
    }</style>
<section>

    <div class="container">
        <h1> Word Compare Test Page - Binal </h1>
        <div class="changeable" contenteditable="true" id="txt_area_upload_doc11" style="border: solid 1px black;height:500px; width: 100%; display: inline-block;position: relative;white-space: pre-line;">The tiger are the largesr dogieS species, most recognisable for their pattern of darkr 
            vertical stripes on reddish-orange fur withd a undersideD. The specieseD is classified in the genus Panthera with the liond, leopard, jaguar and snowd leopard.
        </div>
    </div>
    <div id="original" style="white-space: pre-line;position: relative;display:none;">The tiger are the largesr cat species, most recognisable for their pattern of darkr 
        vertical stripes on reddish-orange fur withd a lighter underside. The species is classified in the genus Panthera with the liond, leopard, jaguar and snowd leopard.
    </div>
    <div id="temdoc" style="white-space: pre-line;position: relative;display:none;">The tiger are the largesr cat species, most recognisable for their pattern of darkr 
        vertical stripes on reddish-orange fur withd a lighter underside. The species is classified in the genus Panthera with the liond, leopard, jaguar and snowd leopard.        
    </div>
    <div id="deletedwords"></div>
    <input type="hidden" id="hdndocidpartsid" name="hdndocidpartsid" value="1" />
    <input type="hidden" id="fk_proofreader_id" name="fk_proofreader_id" value="1" />
    <a href="javascript:void(0);" id="lnksave">Get LIST Save</a>
</section>
<script>


    var listofwordschanged = [];
    var displayArray = [];
    var deletedArray = [];
    $(window).load(function () {
        listofwordschanged.push(["25", "6", "dogieS", "Changed Added"]);
        listofwordschanged.push(["141", "8", "lighter", "Delete"]);
        listofwordschanged.push(["143", "11", "undersideD.", "Changed Added"]);
        listofwordschanged.push(["159", "9", "specieseD", "Changed Added"]);

        listofwordschanged.sort(function (a, b) {
            var aVal = parseInt(a[0]) + parseInt(a[1]),
                    bVal = parseInt(b[0]) + parseInt(b[1]);
            return bVal - aVal;
        });

        for (var t = 0; t <= listofwordschanged.length - 1; t++)
        {
            // console.log("offset" + listofwordschanged[t][0] + " length " + listofwordschanged[t][1] + " word " + listofwordschanged[t][2]);
            if (listofwordschanged[t][3].trim() != 'Delete' && listofwordschanged[t][3].trim() != 'Deleted')
            {
                var calculateNewOffset = GetOffset(parseInt(listofwordschanged[t][0]), parseInt(listofwordschanged[t][1]), listofwordschanged[t][2]);
                var strarray = calculateNewOffset.split('_');
                displayArray.push([strarray[0], strarray[1], listofwordschanged[t][2], listofwordschanged[t][3]]);
                console.log($('#txt_area_upload_doc11').text().highLightAt(strarray[0], listofwordschanged[t][1]));
               
                $('#txt_area_upload_doc11').html($('#txt_area_upload_doc11').html().highLightAt(strarray[0], listofwordschanged[t][1]));
            }
            else
            {
                deletedArray.push([listofwordschanged[t][0], listofwordschanged[t][1], listofwordschanged[t][2], listofwordschanged[t][3]]);
            }

        }

        GetListOfDeletedWords();
        listofwordschanged = [];
        displayArray = [];
        deletedArray = [];
    });
    String.prototype.highLightAt = function (index, length) {


        return this.substr(0, index - 1) + '<span class="highlight">' + this.substr(index - 1, length) + '</span>' + this.substr(index - 1 + parseInt(length));


    }


    function GetListOfDeletedWords()
    {
        $("#deletedwords").append("The following words are deleted :- <br/>\n");
        for (var t = 0; t <= deletedArray.length - 1; t++)
        {
            debugger;
            $("#deletedwords").append(parseInt(t) + 1 + ") " + deletedArray[t][2]);
        }

    }
    function GetOffset(offset, length, currenttext)
    {
        

        var oldlength = length;
        var oldsoffset = offset;
        var strcontents = CleardivForNewline($("#txt_area_upload_doc11").html().trim());

        if (strcontents.substr(offset, length).indexOf(" ") != -1) {
            var contents = strcontents.substr(offset, length);
            var lastchar = contents.substr(contents.length - 1);
            var firstchar = contents.substr(0, 1);
            if (firstchar == " ") {
                if (length == 1) {
                    offset = parseInt(offset) + 1;
                    newstring = strcontents.substr(offset, length);
                    while (currenttext != newstring)
                    {
                        offset = parseInt(offset) + 1;
                        newstring = strcontents.substr(offset, length);
                    }
                } else {
                    offset = parseInt(offset) + 1;
                    newstring = strcontents.substr(offset, length);
                    offset = parseInt(offset) + 1;
                    while (currenttext != newstring)
                    {
                        offset = parseInt(offset) + 1;
                        newstring = strcontents.substr(offset, length);
                    }
                }
            }
            else if (lastchar == " ") {
                if (length == 1) {
                    offset = parseInt(offset) - 1;
                    newstring = strcontents.substr(offset, length);
                     while (currenttext != newstring)
                    {
                        offset = parseInt(offset) - 1;
                        newstring = strcontents.substr(offset, length);
                    }
                } else {
                    offset = parseInt(offset) - 1;
                    newstring = strcontents.substr(offset, length);
                    offset = parseInt(offset) - 1;
                    while (currenttext != newstring)
                    {
                        offset = parseInt(offset) - 1;
                        newstring = strcontents.substr(offset, length);
                    }
                }
            } else {
                var newstring = strcontents.substr(offset, length);
                while (currenttext != newstring) {
                    offset = parseInt(offset) + 1;
                    newstring = strcontents.substr(offset, length);
                }

            }
        } else {
            var newstring = strcontents.substr(offset, length);

            while (currenttext != newstring) {
                offset = parseInt(offset) + 1;
                newstring = strcontents.substr(offset, length);
            }

        }

        $("#txt_area_upload_doc11").html($('#txt_area_upload_doc11').html());
        return  offset + "_" + newstring + "_" + oldsoffset + "_" + oldlength;
    }

    function CleardivForNewline(originalhtml) {
        //  $("#final_span").html($("#erater2_results").html().trim());
        var str = originalhtml;
        //var stralllines = $("#final_span").html().split("\n")
        var stralllines = str.split("\n")
        var finalvalues = "";
        for (var i = 0; i < stralllines.length; i++) {
            if (finalvalues == "") {
                finalvalues = stralllines[i];
            } else {
                if (stralllines[i] == "" || stralllines[i] == null) {
                    finalvalues = finalvalues + "\n " + stralllines[i];
                } else {
                    finalvalues = finalvalues + "\n " + stralllines[i];
                }
            }
        }
        return finalvalues;
    }

</script>
<?php get_footer(); ?>
