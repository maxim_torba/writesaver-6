<?php

/*
 * Template Name: Proofreader Dashboard
 */

$current_user = wp_get_current_user();
$user_roles_array = $current_user->roles;
$user_role = array_shift($user_roles_array);
if (!is_user_logged_in() || $user_role != "proofreader") {
    echo '<script>window.location.href="' . get_site_url() . '"</script>';
    exit;
}

get_header();
?>

<section>
    <div class="breadcum">
        <div class="container">
            <div class="page_title">
                <h1>Dashboard</h1>
            </div>
        </div>
    </div>
</section>   
<?php

if (have_posts()) :
    while (have_posts()) : the_post();
        the_content();
    endwhile;
endif;
?>
<?php get_footer(); ?>


<script>

    $(document).on("click", ".proof_dashboard_accor .wpb_wrapper .vc_toggle_simple", function () {
        var id = $(this).attr("id");
        $(".proof_dashboard_accor .wpb_wrapper .vc_toggle_simple:not(#" + id + ")").removeClass('vc_toggle_active');
        $(".proof_dashboard_accor .wpb_wrapper .vc_toggle_simple:not(#" + id + ")").find(".vc_toggle_content").hide();
    });

</script>